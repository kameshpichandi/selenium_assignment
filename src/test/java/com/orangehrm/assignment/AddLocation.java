package com.orangehrm.assignment;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddLocation {

	public static void main(String[] args) throws AWTException {
		
		WebDriver driver = new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.manage().window().maximize();

		driver.findElement(By.name("username")).sendKeys("Admin");
		driver.findElement(By.name("password")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[text()=' Login ']")).click();
		driver.findElement(By.xpath("//ul[@class='oxd-main-menu']//a[@href='/web/index.php/admin/viewAdminModule']")).click();
		driver.findElement(By.xpath("//header[@class='oxd-topbar']//li[3]")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Locations')]")).click();
		driver.findElement(By.xpath("//button[@class='oxd-button oxd-button--medium oxd-button--secondary']/i")).click();
		driver.findElement(By.xpath("//div[@class='orangehrm-card-container']//div[1]//div[1]//div[1]//div[1]//div[2]//input[1]")).sendKeys("Kamesh");
		driver.findElement(By.xpath("(//div[@class='oxd-form-row'])[2]/child::div/div[1]/div/div[2]/input")).sendKeys("Chennai");
		driver.findElement(By.xpath("(//div[@class='oxd-form-row'])[2]/child::div/div[2]/div/div[2]/input")).sendKeys("Tamil Nadu");
		driver.findElement(By.xpath("(//div[@class='oxd-form-row'])[2]/child::div/div[3]/div/div[2]/input")).sendKeys("600021");
		driver.findElement(By.xpath("(//div[@class='oxd-form-row'])[2]/child::div/div[4]/div/div[2]/div/div/div[2]/i")).click();
		
		Robot br=new Robot();
		br.keyPress(KeyEvent.VK_DOWN);
		br.keyRelease(KeyEvent.VK_DOWN);
		br.keyPress(KeyEvent.VK_DOWN);
		br.keyRelease(KeyEvent.VK_DOWN);
		br.keyPress(KeyEvent.VK_DOWN);
		br.keyRelease(KeyEvent.VK_DOWN);
		br.keyPress(KeyEvent.VK_DOWN);
		br.keyRelease(KeyEvent.VK_DOWN);
		br.keyPress(KeyEvent.VK_DOWN);
		br.keyRelease(KeyEvent.VK_DOWN);
		br.keyPress(KeyEvent.VK_DOWN);
		br.keyRelease(KeyEvent.VK_DOWN);
		br.keyPress(KeyEvent.VK_ENTER);
		br.keyRelease(KeyEvent.VK_ENTER);
		
		driver.findElement(By.xpath("(//div[@class='oxd-form-row'])[2]/child::div/div[5]/div/div[2]/input")).sendKeys("9876543210");
		driver.findElement(By.xpath("//div[@class='oxd-form-actions']/child::button[2]")).click();
		
		String title="OrangeHRM";
		
		if(driver.getTitle().equals(title)) {
			
			System.out.println("Test case pass");
		}
		else {
			
			System.out.println("Test case fail");
		}
		
		driver.quit();
		
		

		
		
		
		
		
		
		

	}

}
