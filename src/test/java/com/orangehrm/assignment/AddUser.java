package com.orangehrm.assignment;

import java.awt.AWTException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AddUser {

	public static void main(String[] args) throws AWTException, InterruptedException {

		WebDriver driver = new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.manage().window().maximize();

		driver.findElement(By.name("username")).sendKeys("Admin");
		driver.findElement(By.name("password")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[text()=' Login ']")).click();

		driver.findElement(By.xpath("//ul[@class='oxd-main-menu']//a[@href='/web/index.php/admin/viewAdminModule']"))
				.click();
		driver.findElement(By.xpath("//button[@class='oxd-button oxd-button--medium oxd-button--secondary']")).click();

		driver.findElement(By.xpath("(//div[@class='oxd-select-text--after'])[1]/i")).click();

		Actions ac = new Actions(driver);
		ac.keyDown(Keys.ARROW_DOWN);
		ac.keyUp(Keys.ARROW_DOWN);
		ac.keyDown(Keys.ARROW_DOWN);
		ac.keyUp(Keys.ARROW_DOWN);
		ac.keyDown(Keys.ENTER);
		ac.keyUp(Keys.ENTER);
		ac.perform();
		driver.findElement(By.xpath("//input[@placeholder='Type for hints...']")).sendKeys("jo");
		Thread.sleep(3000);
		ac.keyDown(Keys.ARROW_DOWN);
		ac.keyUp(Keys.ARROW_DOWN);
		ac.keyDown(Keys.ARROW_DOWN);
		ac.keyUp(Keys.ARROW_DOWN);
		ac.keyDown(Keys.ARROW_DOWN);
		ac.keyUp(Keys.ARROW_DOWN);
		ac.keyDown(Keys.ARROW_DOWN);
		ac.keyUp(Keys.ARROW_DOWN);
		ac.keyDown(Keys.ENTER);
		ac.keyUp(Keys.ENTER);
		ac.perform();
		driver.findElement(By.xpath("(//div[@class='oxd-select-text--after'])[2]/i")).click();
		ac.keyDown(Keys.ARROW_DOWN);
		ac.keyUp(Keys.ARROW_DOWN);
		ac.keyDown(Keys.ARROW_DOWN);
		ac.keyUp(Keys.ARROW_DOWN);
		ac.keyDown(Keys.ENTER);
		ac.keyUp(Keys.ENTER);
		ac.perform();
		WebElement uname = driver.findElement(By.xpath(
				"//div[@class='oxd-form-row']//div[@class='oxd-grid-2 orangehrm-full-width-grid']//div[@class='oxd-grid-item oxd-grid-item--gutters']//div[@class='oxd-input-group oxd-input-field-bottom-space']//div//input[@class='oxd-input oxd-input--active']"));
		uname.sendKeys("RavinRaghul kumar");

		driver.findElement(By.xpath("//div[@class='oxd-grid-item oxd-grid-item--gutters user-password-cell']//div[@class='oxd-input-group oxd-input-field-bottom-space']//div//input[@type='password']")).sendKeys("Kamesh@1430");
		driver.findElement(By.xpath("//div[@class='oxd-grid-item oxd-grid-item--gutters']//div[@class='oxd-input-group oxd-input-field-bottom-space']//div//input[@type='password']")).sendKeys("Kamesh@1430");
		driver.findElement(By.xpath("//button[@type='submit']")).click();

		 driver.findElement(By.xpath("//p[@class='oxd-userdropdown-name']")).click();
		 driver.findElement(By.xpath("//a[text()='Logout']")).click();

		
		  String title = "OrangeHRM";
		  
		  if (driver.getTitle().equals(title)) {
		  
		  System.out.println("Test case pass"); 
		  
		  } 
		  else {
		  
		  System.out.println("Test case fail"); }
		 
		 driver.quit();

	}

}
